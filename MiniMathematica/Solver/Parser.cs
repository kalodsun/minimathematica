﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniMathematica
{
    class Parser
    {
        private const int BRACKET_PRIORITY = 100;

        private const char SEPARATOR = ',';
        private const char DECIMAL = '.';

        private Node firstNode, currentNode;

        enum CharType { Letter, Digit, Space, Decimal, Separator, OperatorSymbol, Bracket, Invalid }

        public Node Parse(String input, List<OperatorNode> opList)
        {
            firstNode = new SeparatorNode(-1);
            currentNode = firstNode;

            input = input.ToLowerInvariant();
            input += " ";

            CharType beginningType = CharType.Invalid, currentType;
            String currentSubstr = "";
            int basePriority = 0;

            for (int i = 0; i < input.Length; i++)
            {
                char cval = input[i];
                currentType = GetCharType(cval);

                bool shouldEnd = false;
                switch (beginningType)//end or append+continue the currently built node if needed
                {
                    case CharType.Invalid://not currently building a node
                        break;
                    case CharType.Digit:
                    case CharType.Decimal:
                        if (currentType != CharType.Decimal && currentType != CharType.Digit)
                            shouldEnd = true;
                        else
                        {
                            currentSubstr += cval;
                            continue;
                        }
                        break;
                    case CharType.Letter:
                        if (currentType == CharType.Space || currentType == CharType.Separator || currentType == CharType.Bracket || currentType == CharType.OperatorSymbol)
                            shouldEnd = true;
                        else
                        {
                            currentSubstr += cval;
                            continue;
                        }
                        break;

                    //last two are single-char nodes
                    case CharType.OperatorSymbol:
                        shouldEnd = true;
                        break;
                    case CharType.Separator:
                        shouldEnd = true;
                        break;
                }

                if (shouldEnd)
                {
                    currentNode.Attach(BuildNode(currentSubstr, beginningType, currentNode.type, basePriority));
                    currentNode = currentNode.next;

                    if (currentNode.type == NodeType.Operator)
                        opList.Add((OperatorNode)currentNode);

                    beginningType = CharType.Invalid;
                }

                switch (currentType)
                {
                    case CharType.Bracket:
                        if (cval == '(')
                            basePriority += BRACKET_PRIORITY;
                        else
                            basePriority -= BRACKET_PRIORITY;

                        if (basePriority < 0)
                            throw new SolverException("Bad brackets at position: " + (i + 1).ToString());
                        break;
                    case CharType.Invalid:
                        throw new SolverException("Invalid character at position: " + (i + 1).ToString());
                    case CharType.Space:
                        break;
                    case CharType.Decimal:
                    case CharType.Digit:
                    case CharType.Letter:
                    case CharType.OperatorSymbol:
                    case CharType.Separator:
                        currentSubstr = cval.ToString();
                        beginningType = currentType;
                        break;
                }
            }

            if (basePriority != 0)
                throw new SolverException("Mismatched closing brackets");

            return firstNode;
        }

        private Node BuildNode(String substr, CharType beginningType, NodeType preType, int basePriority)
        {
            switch (beginningType)
            {
                case CharType.OperatorSymbol:
                    return new OperatorNode(substr, basePriority + GetOperatorPriority(substr, !(preType == NodeType.Number)));
                case CharType.Letter:
                    int priority = GetOperatorPriority(substr, false);

                    if (priority == -1)
                        throw new SolverException("Unknown function: \"" + substr + "\"");

                    return new OperatorNode(substr, basePriority + priority);
                case CharType.Digit:
                case CharType.Decimal:
                    return new NumberNode(Double.Parse(substr, System.Globalization.CultureInfo.InvariantCulture));
                case CharType.Separator:
                    return new SeparatorNode(basePriority);
            }

            throw new SolverException("Can't build node - Wrong beginning type!");
        }

        private static int GetOperatorPriority(String op, bool preceededByOp)
        {
            switch (op)
            {
                case "-":
                case "+":
                    if (!preceededByOp)
                        return 2;
                    else
                        return 8;
                case "*":
                case "/":
                case "%":
                    return 4;
                case "^":
                    return 6;
                case "log":
                case "log10":
                case "ln":
                case "sin":
                case "cos":
                case "tan":
                case "asin":
                case "acos":
                case "atan":
                case "ctan":
                case "pow":
                case "sqrt":
                case "exp":
                case "avg":
                case "root":
                case "max":
                case "min":
                    return 10;
                case "pi":
                case "e":
                    return 12;
            }

            return -1;
        }

        private static CharType GetCharType(char c)
        {
            if (c == SEPARATOR)
                return CharType.Separator;
            if (c == DECIMAL)
                return CharType.Decimal;
            if (c == '(' || c == ')')
                return CharType.Bracket;
            if ("+-*/^%".Contains(c))
                return CharType.OperatorSymbol;
            if (c == ' ')
                return CharType.Space;
            if (Char.IsLetter(c))
                return CharType.Letter;
            if (Char.IsDigit(c))
                return CharType.Digit;

            return CharType.Invalid;
        }
    }
}
