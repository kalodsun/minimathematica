﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniMathematica
{
    class SolverException : Exception
    {
        public SolverException(String message)
            : base(message)
        {
        }
    }
}
