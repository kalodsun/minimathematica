﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniMathematica
{
    class ExpressionSolver
    {
        Node firstNode;

        Parser parser;

        public bool detailed = false;

        public ExpressionSolver()
        {
            parser = new Parser();
        }

        public double Solve(String input)
        {
            List<OperatorNode> opList = new List<OperatorNode>();

            firstNode = parser.Parse(input, opList);

            opList.Sort((OperatorNode a, OperatorNode b) =>
            {
                if (a.priority > b.priority)
                    return -1;
                else if (a.priority == b.priority)
                    return 0;

                return 1;
            });

            if(detailed)
                PrintAllNodes();

            for (int i = 0; i < opList.Count; i++)
            {
                Evaluate(opList[i]);

                if (detailed)
                    PrintAllNodes();
            }

            return ((NumberNode)firstNode.next).value;
        }

        private void Evaluate(OperatorNode op)
        {
            switch(op.op)
            {
                case "+":
                    ExecuteOperator((x, y) => x + y, op, true);
                    break;
                case "-":
                    ExecuteOperator((x, y) => x - y, op, true);
                    break;
                case "*":
                    ExecuteOperator((x, y) => x * y, op, false);
                    break;
                case "/":
                    ExecuteOperator((x, y) => x / y, op, false);
                    break;
                case "%":
                    ExecuteOperator((x, y) => x % y, op, false);
                    break;
                case "^":
                    ExecuteOperator((x, y) => Math.Pow(x, y), op, false);
                    break;
                case "sin":
                    ExecuteFunction((List<double> x) => Math.Sin(x[0]), op, 1);
                    break;
                case "cos":
                    ExecuteFunction((List<double> x) => Math.Cos(x[0]), op, 1);
                    break;
                case "tan":
                    ExecuteFunction((List<double> x) => Math.Tan(x[0]), op, 1);
                    break;
                case "ctan": //All hail the Yngir!
                    ExecuteFunction((List<double> x) => 1/Math.Tan(x[0]), op, 1);
                    break;
                case "asin":
                    ExecuteFunction((List<double> x) => Math.Asin(x[0]), op, 1);
                    break;
                case "acos":
                    ExecuteFunction((List<double> x) => Math.Acos(x[0]), op, 1);
                    break;
                case "atan":
                    ExecuteFunction((List<double> x) => Math.Atan(x[0]), op, 1);
                    break;
                case "log":
                    ExecuteFunction((List<double> x) => Math.Log(x[1], x[0]), op, 2);
                    break;
                case "pow":
                    ExecuteFunction((List<double> x) => Math.Pow(x[0], x[1]), op, 2);
                    break;
                case "root":
                    ExecuteFunction((List<double> x) => Math.Pow(x[0], 1.0 / x[1]), op, 2);
                    break;
                case "sqrt":
                    ExecuteFunction((List<double> x) => Math.Sqrt(x[0]), op, 1);
                    break;
                case "exp":
                    ExecuteFunction((List<double> x) => Math.Exp(x[0]), op, 1);
                    break;
                case "ln":
                    ExecuteFunction((List<double> x) => Math.Log(x[0]), op, 1);
                    break;
                case "log10":
                    ExecuteFunction((List<double> x) => Math.Log(x[0], 10), op, 1);
                    break;
                case "avg":
                    ExecuteFunction((List<double> x) =>
                    {
                        double sum = 0.0;

                        foreach (double d in x)
                            sum += d;

                        return sum/x.Count;
                    }, op, -1);
                    break;
                case "max":
                    ExecuteFunction((List<double> x) =>
                    {
                        double max = Double.MinValue;

                        foreach (double d in x)
                            if (max < d)
                                max = d;

                        return max;
                    }, op, -1);
                    break;
                case "min":
                    ExecuteFunction((List<double> x) => {
                        double min = Double.MaxValue;

                        foreach (double d in x)
                            if (min > d)
                                min = d;

                        return min;
                    }, op, -1);
                    break;
                case "pi":
                    PlaceConstant(Math.PI, op);
                    break;
                case "e":
                    PlaceConstant(Math.E, op);
                    break;
            }
        }

        delegate double OperatorBody(double l, double r);
        delegate double FunctionBody(List<double> args);

        private static void ExecuteOperator(OperatorBody body, OperatorNode op, bool canBeUnary)
        {
            double l = 0.0, r = 0.0;
            bool unary = false;

            if (op.prev == null || op.prev.type != NodeType.Number)
            {
                if(canBeUnary)
                    unary = true;
                else
                    throw new SolverException("Operator " + op.op + " can't be unary.");
            }

            if (op.next == null || op.next.type != NodeType.Number)
                throw new SolverException("Operator " + op.op + " has no second operand.");

            if(!unary)
                l = ((NumberNode)op.prev).value;
            r = ((NumberNode)op.next).value;

            ((NumberNode)op.next).value = body(l, r);
            Relink(op, unary ? 1 : 2, 1);
        }

        private static void ExecuteFunction(FunctionBody body, OperatorNode op, int argumentCount)
        {
            List<double> args = new List<double>();

            NumberNode nn = ((NumberNode)op.next);
            args.Add(nn.value);
            int cnt = 1;

            while (nn.next != null && nn.next.type == NodeType.Separator && ((SeparatorNode)nn.next).basePriority > op.priority)
            {
                nn = (NumberNode)nn.next.next;
                args.Add(nn.value);
                cnt++;
            }

            if (argumentCount != -1 && cnt != argumentCount)
                throw new SolverException("Wrong argument number in function " + op.op);

            nn.value = body(args);

            Relink(op, 1, cnt * 2 - 1);
        }

        private static void PlaceConstant(double val, Node node)
        {
            NumberNode nn = new NumberNode(val);

            nn.prev = node.prev;
            nn.next = node.next;

            if (node.next != null)
                node.next.prev = nn;
            if (node.prev != null)
                node.prev.next = nn;
        }

        private void PrintAllNodes()
        {
            Node n = firstNode.next;

            do
            {
                switch(n.type)
                {
                    case NodeType.Number:
                        Console.Write(((NumberNode)n).value.ToString("F"));
                        break;
                    case NodeType.Operator:
                        Console.Write(((OperatorNode)n).op);
                        break;
                    case NodeType.Separator:
                        Console.Write(",");
                        break;
                }

                Console.Write(" ");

                n = n.next;
            } while (n != null);

            Console.Write("\r\n");
        }

        private static void Relink(OperatorNode node, int stepsPrev, int stepsNext)
        {
            if(stepsPrev < 1 || stepsNext < 1)
                throw new SolverException("Invalid relink parameters: " + stepsPrev + " " + stepsNext);

            Node p = node.prev, n = node.next;
            for (int i = 1; i < stepsPrev; i++)
                p = p.prev;

            for (int i = 1; i < stepsNext; i++)
                n = n.next;

            p.next = n;

            if(n != null)
                n.prev = p;
        }
    }
}
