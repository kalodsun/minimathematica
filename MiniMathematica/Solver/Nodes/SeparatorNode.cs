﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniMathematica
{
    class SeparatorNode : Node
    {
        public int basePriority;

        public SeparatorNode(int bp)
        {
            basePriority = bp;
            type = NodeType.Separator;
        }
    }
}
