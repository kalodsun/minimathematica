﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniMathematica
{
    enum NodeType { Number, Operator, Separator}

    abstract class Node
    {
        public NodeType type;
        public Node next, prev;

        public void Attach(Node node)
        {
            this.next = node;
            node.prev = this;
        }
    }
}
