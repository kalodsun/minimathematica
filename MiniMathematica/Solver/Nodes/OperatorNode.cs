﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniMathematica
{
    class OperatorNode : Node
    {
        public int priority;
        public String op;

        public OperatorNode(String op, int pri)
        {
            this.op = op;
            priority = pri;
            type = NodeType.Operator;
        }
    }
}
