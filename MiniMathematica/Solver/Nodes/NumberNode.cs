﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniMathematica
{
    class NumberNode : Node
    {
        public double value;

        public NumberNode(double val)
        {
            type = NodeType.Number;
            value = val;
        }
    }
}
