﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace MiniMathematica
{
    public partial class InterfaceForm : Form
    {
        ExpressionSolver solver = new ExpressionSolver();

        public InterfaceForm()
        {
            InitializeComponent();
        }

        private void InterfaceForm_Load(object sender, EventArgs e)
        {
            Console.SetOut(new TextBoxWriter(txtLog));
        }

        private class TextBoxWriter : TextWriter
        {
            TextBox output;

            public TextBoxWriter(TextBox tb)
            {
                output = tb;
            }

            public override void Write(char value)
            {
                base.Write(value);
                output.AppendText(value.ToString());
            }

            public override Encoding Encoding
            {
                get { return System.Text.Encoding.UTF8; }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtInput.Text == "")
            {
                Console.WriteLine("Input is empty!");
                return;
            }

            try
            {
                txtAnswer.Text = solver.Solve(txtInput.Text).ToString();
            }
            catch (SolverException ex)
            {
                Console.WriteLine("ERROR: " + ex.Message);
            }
        }

        private void chkDetailed_CheckedChanged(object sender, EventArgs e)
        {
            solver.detailed = chkDetailed.Checked;
        }

        private void txtInput_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                button1_Click(sender, e);

            if (e.Control && e.KeyCode == Keys.D)
                chkDetailed.Checked = !chkDetailed.Checked;
        }
    }
}
