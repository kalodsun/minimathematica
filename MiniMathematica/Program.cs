﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniMathematica
{
    class Program
    {
        static ExpressionSolver solver = new ExpressionSolver();

        static void Main(string[] args)
        {
            //CallSolve("(2+((3)))");
            //CallSolve("5 + sin(pi) / pow(2, 10) - log(e, pow(e, sqrt(4)))");
            //CallSolve("3+min(7,9,12,max(5/4,1),100)");

            Application.Run(new InterfaceForm());

            //String input = Console.ReadLine();
        }

        private static void CallSolve(String expr)
        {
            try
            {
                Console.WriteLine(solver.Solve(expr));
            }
            catch (SolverException ex)
            {
                Console.WriteLine("ERROR: " + ex.Message);
            }
        }
    }
}
